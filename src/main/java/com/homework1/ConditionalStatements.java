package main.java.com.homework1;

public class ConditionalStatements {
    public int countIfaEven(int number1, int number2) {
        int result;
        if (number1 % 2 == 0) {
            result = number1 * number2;
        }
        else {
            result = number1 + number2;
        }
        return result;
    }

    public String defineQuarter(int pointX, int pointY) {
        String pointPosition;
        if (pointX > 0 & pointY > 0) {
            pointPosition = "Первая четверть";
        }
        else if (pointX < 0 & pointY > 0) {
            pointPosition = "Вторая четверть";
        }
        else if (pointX < 0 & pointY < 0) {
            pointPosition = "Третья четверть";
        }
        else if (pointX > 0 & pointY < 0) {
            pointPosition = "Четвертая четверть";
        }
        else if (pointX == 0 & pointY == 0) {
            pointPosition = "Начало координат";
        }
        else if (pointX == 0) {
            pointPosition = "Ось X";
        }
        else {
            pointPosition = "Ось Y";
        }
        return  pointPosition;
    }

    public int countSumPositiveNums(int number1, int number2, int number3) {
        int sum = 0;
        if (number1 <= 0 & number2 <= 0 & number3 <= 0) {
            sum = -1; //Возвращает -1, если все числа отрицательные
        } else {
            if (number1 > 0) {
                sum += number1;
            }
            if (number2 > 0) {
                sum += number2;
            }
            if (number3 > 0) {
                sum += number3;
            }
        }
        return  sum;
    }

    public double countStatement(double a, double b, double c) {
        final double three = 3;
        double result;
        double sum = a + b + c;
        double multiplication = a * b * c;
        if (sum >= multiplication) {
            result = sum + three;
        }
        else{
            result = multiplication + three;
        }
        return result;
    }

    public String assessStudent(int ranking) {
        String assessment;
        if (ranking < 0 || ranking > 100) {
            assessment = "Некорректный ввод";
        }
        else if (ranking <= 19) {
            assessment = "F";
        }
        else if (ranking <= 39) {
            assessment = "E";
        }
        else if (ranking <= 59) {
            assessment = "D";
        }
        else if (ranking <= 74) {
            assessment = "C";
        }
        else if (ranking <= 89) {
            assessment = "B";
        }
        else{
            assessment = "A";
        }
        return assessment;
    }
}
