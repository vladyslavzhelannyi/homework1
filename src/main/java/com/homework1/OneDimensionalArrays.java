package main.java.com.homework1;

public class OneDimensionalArrays {
    public int getMinItem(int[] array){
        int min = array[0];
        for(int i = 1; i < array.length; i++){
            if(array[i] < min) min = array[i];
        }
        return min;
    }

    public int getMaxItem(int[] array){
        int max = array[0];
        for(int i = 1; i < array.length; i++){
            if(array[i] > max) max = array[i];
        }
        return max;
    }

    public int getMinItemIndex(int[] array){
        int min = array[0];
        int minIndex = 0;
        for(int i = 1; i < array.length; i++){
            if(array[i] < min){
                min = array[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    public int getMaxItemIndex(int[] array){
        int max = array[0];
        int maxIndex = 0;
        for(int i = 1; i < array.length; i++){
            if(array[i] > max){
                max = array[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    public int countSumOfNumsWithUnevenIndex(int[] array){
        int sumUnevenIndex = 0;
        for(int i = 0; i < array.length; i += 2) {
            sumUnevenIndex += array[i];
        }
        return sumUnevenIndex;
    }

    public int[] reverseArray(int[] array){
        int[] reversedArray = new int[array.length];
        for(int i = 0; i < array.length; i++){
            reversedArray[i] = array[array.length -1 - i];
        }
        return reversedArray;
    }

    public int countUnevenNumQuantity(int[] array){
        int numUnevenItems = 0;
        for(int i = 0; i < array.length; i++){
            if(array[i] % 2 != 0) numUnevenItems += 1;
        }
        return numUnevenItems;
    }

    public int[] changeArrayHalfs(int[] array){
        int arrayLength = array.length;
        boolean isEvenNumInd = arrayLength % 2 == 0;
        int[] arrayIntermediate = new int[arrayLength];
        if(isEvenNumInd){
            for(int i = 0; i < arrayLength / 2; i++){
                arrayIntermediate[i] = array[arrayLength / 2 + i];
                arrayIntermediate[array.length / 2 + i] = array[i];
            }
        }
        else{
            for(int i = 0; i < arrayLength / 2; i++){
                arrayIntermediate[i] = array[arrayLength / 2 + i + 1];
                arrayIntermediate[array.length / 2 + i + 1] = array[i];
            }
            arrayIntermediate[arrayLength / 2] = array[arrayLength / 2];
        }
        for(int i = 0; i < arrayLength; i++){
            array[i] = arrayIntermediate[i];
        }
        return array;
    }

    public int[] sortArrayBubble(int[] array){
        boolean notSorted = true;
        while (notSorted) {
            notSorted = false;
            for(int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int x = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = x;
                    notSorted = true;
                }
            }
        }
        return array;
    }

    public int[] sortArrayInsert(int[] array){
        for(int i = 1; i < array.length; i++){
            int itemToInsert = array[i];
            int j = i - 1;
            while(j >= 0 && array[j] > itemToInsert){
                array[j + 1] = array[j];
                j -= 1;
            }
            array[j + 1] = itemToInsert;
        }
        return array;
    }

    public int[] sortArraySelect(int[] array){
        for(int i = 0; i < array.length - 1; i++){
            int theLowestIndex = i;
            for(int j = i + 1; j < array.length; j++){
                if(array[theLowestIndex] > array[j]){
                    theLowestIndex = j;
                }
            }
            int x = array[i];
            array[i] = array[theLowestIndex];
            array[theLowestIndex] = x;
        }
        return array;
    }

    public int[] sortArrayHeap(int[] array){
        int arrayLength = array.length;
        if(arrayLength < 2){
            return array;
        }
        for(int i = arrayLength / 2 - 1; i >= 0; i--){
            buildMaxHeap(array, arrayLength, i);
        }

        for(int i = arrayLength - 1; i > 0; i--){
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            array = buildMaxHeap(array, i, 0);
        }
        return array;
    }

    private int[] buildMaxHeap(int[] array, int arrayLength, int index){
        int largestIndex  = index;
        int leftLeafIndex = index * 2 + 1;
        int rightLeafIndex = index * 2 + 2;

        if(leftLeafIndex < arrayLength && array[largestIndex] < array[leftLeafIndex]){
            largestIndex = leftLeafIndex;
        }

        if(rightLeafIndex < arrayLength && array[largestIndex] < array[rightLeafIndex]){
            largestIndex = rightLeafIndex;
        }

        if(largestIndex != index){
            int temp = array[index];
            array[index] = array[largestIndex];
            array[largestIndex] = temp;

            array = buildMaxHeap(array, arrayLength, largestIndex);
        }
        return array;
    }

    public int[] sortArrayMerge(int[] array){
        int arrLen = array.length;
        //возврат базового (единичного) массива
        if(arrLen < 2){
            return array;
        }
        //создание двух частей массива
        int[] arrLeft = new int[arrLen / 2];
        int[] arrRight = new int[arrLen - arrLen / 2];
        //заполнение двух частей массива
        for(int i = 0; i < arrLeft.length; i++){
            arrLeft[i] = array[i];
        }
        for(int i = 0; i < arrRight.length; i++){
            arrRight[i] = array[i + arrLen / 2];
        }
        //рекурсивный вызов для сортировки двух частей массива
        int[] arrLeftSorted = sortArrayMerge(arrLeft);
        int[] arrRightSorted = sortArrayMerge(arrRight);
        //объединение двух массивов с сортировкой
        int[] sortedArray = new int[arrLen];
        int leftI = 0;
        int rightI = 0;
        boolean leftEnd = false;
        boolean rightEnd = false;
        for(int i = 0; i < array.length; i++){
            if(leftEnd){
                sortedArray[i] = arrRightSorted[rightI];
                rightI += 1;
            }
            else if(rightEnd){
                sortedArray[i] = arrLeftSorted[leftI];
                leftI += 1;
            }
            else if(arrLeftSorted[leftI] < arrRightSorted[rightI]){
                sortedArray[i] = arrLeftSorted[leftI];
                leftI += 1;
                if(leftI == arrLeftSorted.length) leftEnd = true;
            }
            else{
                sortedArray[i] = arrRightSorted[rightI];
                rightI += 1;
                if(rightI == arrRightSorted.length) rightEnd = true;
            }
        }
        return sortedArray;
    }

    public int[] sortArrayQuick(int[] array){
        int arrLen = array.length;
        if (arrLen < 2){
            return array;
        }
        int basic = array[0];
        int[] lessBasic = new int[0];
        int[] moreBasic = new int[0];
        int[] tempArr;
        for (int i = 1; i < arrLen; i++){
            if (array[i] < basic){
                tempArr = lessBasic;
                lessBasic = new int[lessBasic.length + 1];
                for (int j = 0; j < tempArr.length; j++){
                    lessBasic[j] = tempArr[j];
                }
                lessBasic[lessBasic.length - 1] = array[i];
            }
            else{
                tempArr = moreBasic;
                moreBasic = new int[moreBasic.length + 1];
                for (int j = 0; j < tempArr.length; j++){
                    moreBasic[j] = tempArr[j];
                }
                moreBasic[moreBasic.length - 1] = array[i];
            }
        }

        int[] lessSorted = sortArrayQuick(lessBasic);
        int[] moreSorted = sortArrayQuick(moreBasic);

        int[] sortedArray = new int[arrLen];
        if (lessSorted.length != 0){
            for (int i = 0; i < lessSorted.length; i++){
                sortedArray[i] = lessSorted[i];
            }
        }
        sortedArray[lessSorted.length] = basic;
        if (moreSorted.length != 0){
            for (int i = 0; i < moreSorted.length; i++){
                sortedArray[lessSorted.length + i + 1] = moreSorted[i];
            }
        }
        return sortedArray;
    }

    public int[] sortArrayShell(int[] array){
        int n = array.length;
        for (int step = n / 2; step > 0; step /= 2) {
            for (int i = step; i < n; i++) {
                for (int j = i - step; j >= 0 && array[j] > array[j + step]; j -= step) {
                    int x = array[j];
                    array[j] = array[j + step];
                    array[j + step] = x;
                }
            }
        }
        return array;
    }
}
