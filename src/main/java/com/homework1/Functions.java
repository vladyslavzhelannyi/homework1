package main.java.com.homework1;

public class Functions {
    private static final String ZERO = "ноль";
    private static final String ONE = "один";
    private static final String TWO = "два";
    private static final String THREE = "три";
    private static final String FOUR = "четыре";
    private static final String FIVE = "пять";
    private static final String SIX = "шесть";
    private static final String SEVEN = "семь";
    private static final String EIGHT = "восемь";
    private static final String NINE = "девять";
    private static final String TEN = "десять";
    private static final String ELEVEN = "одиннадцать";
    private static final String TWELVE = "двенадцать";
    private static final String THIRTEEN = "тринадцать";
    private static final String FOURTEEN = "четырнадцать";
    private static final String FIFTEEN = "пятнадцать";
    private static final String SIXTEEN = "шестнадцать";
    private static final String SEVENTEEN = "семнадцать";
    private static final String EIGHTEEN = "восемнадцать";
    private static final String NINETEEN = "девятнадцать";
    private static final String TWENTY = "двадцать";
    private static final String THIRTY = "тридцать";
    private static final String FOURTY = "сорок";
    private static final String FIFTY = "пятьдесят";
    private static final String SIXTY = "шестьдесят";
    private static final String SEVENTY = "семьдесят";
    private static final String EIGHTY = "восемьдесят";
    private static final String NINETY = "девяносто";
    private static final String ONE_HUNDRED = "сто";
    private static final String TWO_HUNDREDS = "двести";
    private static final String THREE_HUNDREDS = "триста";
    private static final String FOUR_HUNDREDS = "четыреста";
    private static final String FIVE_HUNDREDS = "пятьсот";
    private static final String SIX_HUNDREDS = "шестьсот";
    private static final String SEVEN_HUNDREDS = "семьсот";
    private static final String EIGHT_HUNDREDS = "восемьсот";
    private static final String NINE_HUNDREDS = "девятьсот";


    public String defineDayByNumber(int number) {
        String dayOfWeek;
        if (number < 1 || number > 7) {
            dayOfWeek = "Неверный номер";
        }
        else{
            String[] daysOfWeek = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"};
            dayOfWeek = daysOfWeek[number - 1];
        }
        return dayOfWeek;
    }
    
    public double countDistance(long xPoint1, long yPoint1, long xPoint2,  long yPoint2){
        double distance = Math.sqrt(Math.pow((xPoint1 - xPoint2), 2) + Math.pow((yPoint1 - yPoint2), 2));
        return distance;
    }
    
    public String convertNumToStr(long num){
        if (num == 0) return "ноль";
        String numStr = "";
        long numBil =num / 1_000_000_000;
        if (numBil != 0){
            numStr += convertHundredsToString(numBil);
            if (numBil % 10 == 1 & numBil % 100 != 11){
                numStr += "миллиард ";
            }
            else if ((numBil % 10 == 2 || numBil % 10 == 3 || numBil % 10 == 4)
                    && numBil % 100 != 12 && numBil % 100 != 13 && numBil % 100 != 14){
                numStr += "миллиарда ";
            }
            else{
                numStr += "миллиардов ";
            }
        }

        long numMil =num % 1_000_000_000 / 1_000_000;
        if (numMil != 0){
            numStr += convertHundredsToString(numMil);
            if (numMil % 10 == 1 & numMil % 100 != 11){
                numStr += "миллион ";
            }
            else if ((numMil % 10 == 2 || numMil % 10 == 3 || numMil % 10 == 4)
                    && numMil % 100 != 12 && numMil % 100 != 13 && numMil % 100 != 14){
                numStr += "миллиона ";
            }
            else{
                numStr += "миллионов ";
            }
        }

        long numTho =num % 1_000_000 / 1_000;
        if (numTho != 0){
            numStr += convertHundredsToString(numTho);
            if (numTho % 10 == 1 & numTho % 100 != 11){
                numStr += "тысяча ";
            }
            else if ((numTho % 10 == 2 || numTho % 10 == 3 || numTho % 10 == 4)
                    && numTho % 100 != 12 && numTho % 100 != 13 && numTho % 100 != 14){
                numStr += "тысячи ";
            }
            else{
                numStr += "тысяч ";
            }
        }

        long numHun =num % 1_000;
        if (numHun != 0){
            numStr += convertHundredsToString(numHun);
        }

        return numStr;
    }

    private String convertHundredsToString(long num){
        int number = (int) num;
        if(number > 999 || number < 0) return "Некорректное число"; //Refactor
        String numStr = "";
        String[] digits = {ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE};
        String[] unusualTens = {TEN, ELEVEN, TWELVE, THIRTEEN, FOURTEEN, FIFTEEN, SIXTEEN, SEVENTEEN, EIGHTEEN, NINETEEN};
        String[] tens = {TWENTY, THIRTY, FOURTY, FIFTY, SIXTY, SEVENTY, EIGHTY, NINETY};
        String[] hundreds = {ONE_HUNDRED, TWO_HUNDREDS, THREE_HUNDREDS, FOUR_HUNDREDS, FIVE_HUNDREDS, SIX_HUNDREDS, SEVEN_HUNDREDS, EIGHT_HUNDREDS, NINE_HUNDREDS};
        if(number / 100 != 0){
            numStr += hundreds[number / 100 - 1] + " ";
        }
        if((number % 100) > 9L && (number % 100) < 20) {
            numStr += unusualTens[number % 10] + " ";
        }
        else{
            if((number % 100) / 10 != 0) numStr += tens[(number % 100) / 10 - 2] + " ";
            if(number % 10 != 0) numStr += digits[number % 10] + " ";
        }
        return numStr;
    }

    public int convertStrToNum(String number){
        int numInt = 0;
        String[] digits = {ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE};
        String[] unusualTens = {TEN, ELEVEN, TWELVE, THIRTEEN, FOURTEEN, FIFTEEN, SIXTEEN, SEVENTEEN, EIGHTEEN, NINETEEN};
        String[] tens = {TWENTY, THIRTY, FOURTY, FIFTY, SIXTY, SEVENTY, EIGHTY, NINETY};
        String[] hundreds = {ONE_HUNDRED, TWO_HUNDREDS, THREE_HUNDREDS, FOUR_HUNDREDS, FIVE_HUNDREDS, SIX_HUNDREDS, SEVEN_HUNDREDS, EIGHT_HUNDREDS, NINE_HUNDREDS};
        String[] numArray = number.split(" ");
        if(number.equals(digits[0])) return 0;
        if(numArray.length == 3){
            for(int i = 0; i < hundreds.length; i++){
                if(numArray[0].equals(hundreds[i])) numInt += (i + 1) * 100;
            }
            for(int i = 0; i < tens.length; i++){
                if(numArray[1].equals(tens[i])) numInt += (i + 2) * 10;
            }
            for(int i = 0; i < digits.length; i++){
                if(numArray[2].equals(digits[i])) numInt += i;
            }
        }
        else if(numArray.length == 1){
            for(int i = 0; i < hundreds.length; i++){
                if(numArray[0].equals(hundreds[i])) numInt += (i + 1) * 100;
            }
            for(int i = 0; i < digits.length; i++){
                if(numArray[0].equals(digits[i])) numInt += i;
            }
            if(numInt != 0) return numInt;
            for(int i = 0; i < unusualTens.length; i++){
                if(numArray[0].equals(unusualTens[i])) numInt += i + 10;
            }
        }
        else{
            boolean haveUnusualTens = false;
            for(int i = 0; i < unusualTens.length; i++){
                if(numArray[1].equals(unusualTens[i])){
                    numInt += i + 10;
                    haveUnusualTens = true;
                }
            }
            for(int i = 0; i < hundreds.length; i++){
                if(numArray[0].equals(hundreds[i])) numInt += (i + 1) * 100;
            }
            if(!haveUnusualTens) {
                for (int i = 0; i < tens.length; i++) {
                    if (numArray[0].equals(tens[i])) numInt += (i + 2) * 10;
                }
                for (int i = 0; i < digits.length; i++) {
                    if (numArray[1].equals(digits[i])) numInt += i;
                }
            }
        }
        return numInt;
    }
}
