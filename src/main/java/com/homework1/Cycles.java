package main.java.com.homework1;

public class Cycles {
    public int findSumEvenFrom1To99(){
        int sumEven = 0;
        for(int number = 1; number <= 99; number++){
            if(number % 2 == 0) sumEven += number;
        }
        return sumEven;
    }

    public boolean defineIsPrime(int number){
        boolean isPrime = true;
        for(int i = 2; i * i <= number; i++){
            if(number % i == 0) isPrime = false;
        }
        return isPrime;
    }

    public int defineRootSequentialSelection(int number){
        int root = 0;
        for(int i = 1; i * i <= number ;i++){
            root = i;
        }
        return root;
    }

    public int defineRootBinarySearch(int number){
        int min = 1;
        int max = number;
        int mid;
        int previous = 1;
        int root = 0;
        boolean haveResult = false;
        while(!haveResult){
            mid = (min + max) / 2;
            if(previous == mid){
                root = previous;
                haveResult = true;
            }
            if(mid * mid == number){
                root = mid;
                haveResult = true;
            }
            if(mid * mid > number){
                max = mid;
            }
            else min = mid;
            previous = mid;
        }
        return root;
    }

    public int calculateFactorial(int number){
        int factorial = 1;
        for(int i = 2; i <= number; i++){
            factorial *= i;
        }
        return factorial;
    }

    public int countSumOfDigits(int number){
        int sumNum = 0;
        for(int i = 10; number / i != 0 || number % i != 0;){
            sumNum += number % i;
            number /= i;
        }
        if(sumNum < 0){
            sumNum *= -1;
        }
        return sumNum;
    }

    public int convertToMirrorNumber(int number){
        int mirrorNumber = 0;
        int digit;
        int copyOfNumber = number;
        int sumOfDigits = 0;
        int maxRank = 1;
        while (copyOfNumber != 0){
            copyOfNumber /= 10;
            sumOfDigits++;
        }
        for(int i = 0; i < sumOfDigits - 1; i++){
            maxRank *= 10;
        }
        for(int rank = maxRank; number != 0; rank /= 10){
            digit = number % 10;
            mirrorNumber += digit * rank;
            number /= 10;
        }
        return mirrorNumber;
    }
}
