package test.java.com.homework1;

import main.java.com.homework1.ConditionalStatements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class ConditionalStatementsTest {
    ConditionalStatements conditionalStatements = new ConditionalStatements();

    static Arguments[] countIfaEvenTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 5, 20),
                Arguments.arguments(3, 5, 8)
        };
    }

    static Arguments[] defineQuarterTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 6, "Первая четверть"),
                Arguments.arguments(-7, 2, "Вторая четверть"),
                Arguments.arguments(-3, -5, "Третья четверть"),
                Arguments.arguments(11, -6, "Четвертая четверть"),
                Arguments.arguments(0, 0, "Начало координат"),
                Arguments.arguments(0, 15, "Ось X"),
                Arguments.arguments(13, 0, "Ось Y"),
        };
    }

    static Arguments[] countSumPositiveNumsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, 5, 6, 14),
                Arguments.arguments(4, -5, 3, 7),
                Arguments.arguments(-8, 5, -7, 5),
                Arguments.arguments(-1, -9, -3, -1),
        };
    }

    static Arguments[] countStatementTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, 5, 6, 93, 0.00001),
                Arguments.arguments(1, 1, 2, 7, 0.00001)
        };
    }

    static Arguments[] assessStudentTestArgs(){
        return new Arguments[] {
                Arguments.arguments(10, "F"),
                Arguments.arguments(28, "E"),
                Arguments.arguments(59, "D"),
                Arguments.arguments(71, "C"),
                Arguments.arguments(89, "B"),
                Arguments.arguments(93, "A"),
                Arguments.arguments(-34, "Некорректный ввод"),
                Arguments.arguments(234, "Некорректный ввод"),
        };
    }

    @ParameterizedTest
    @MethodSource("countIfaEvenTestArgs")
    public void countIfaEvenTest(int number1, int number2, int expected) {
        int result = conditionalStatements.countIfaEven(number1, number2);
        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest
    @MethodSource("defineQuarterTestArgs")
    public void defineQuarterTest(int pointX, int pointY, String expected){
        String result = conditionalStatements.defineQuarter(pointX, pointY);
        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest
    @MethodSource("countSumPositiveNumsTestArgs")
    public void countSumPositiveNumsTest(int number1, int number2, int number3, int expected) {
        int result = conditionalStatements.countSumPositiveNums(number1, number2, number3);
        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest
    @MethodSource("countStatementTestArgs")
    public void countStatementTest(double number1, double number2, double number3, double expected, double error) {
        double result = conditionalStatements.countStatement(number1, number2, number3);
        Assertions.assertEquals(expected, result, error);
    }

    @ParameterizedTest
    @MethodSource("assessStudentTestArgs")
    public void assessStudentTest(int mark, String expected) {
        String result = conditionalStatements.assessStudent(mark);
        Assertions.assertEquals(expected, result);
    }
}
