package test.java.com.homework1;

import main.java.com.homework1.Functions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class FunctionsTest {
    Functions functions = new Functions();

    static Arguments[] defineDayByNumberTestArgs(){
        return new Arguments[]{
          Arguments.arguments(1, "Понедельник"),
          Arguments.arguments(2, "Вторник"),
          Arguments.arguments(3, "Среда"),
          Arguments.arguments(4, "Четверг"),
          Arguments.arguments(5, "Пятница"),
          Arguments.arguments(6, "Суббота"),
          Arguments.arguments(7, "Воскресенье"),
          Arguments.arguments(-4, "Неверный номер"),
          Arguments.arguments(8, "Неверный номер"),
        };
    }

    static Arguments[] countDistanceTestArgs(){
        return new Arguments[]{
                Arguments.arguments(6L, 6L, 9L, 10L, 0.000001, 5.0),
                Arguments.arguments(21_346_567_107L, 21_346_567_107L, 21_346_567_110L, 21_346_567_111L, 0.000001, 5.0)
        };
    }

    static Arguments[] convertNumToStrTestArgs(){
        return new Arguments[]{
                Arguments.arguments(34_567_001L,
                        "тридцать четыре миллиона пятьсот шестьдесят семь тысяч один "),
                Arguments.arguments(701_003_405_087L,
                        "семьсот один миллиард три миллиона четыреста пять тысяч восемьдесят семь "),
        };
    }

    static Arguments[] convertStrToNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments("шестьсот семь", 607),
                Arguments.arguments("ноль", 0),
                Arguments.arguments("семьсот тринадцать", 713),
                Arguments.arguments("пятьсот сорок девять", 549),
                Arguments.arguments("шестьсот", 600),
                Arguments.arguments("восемьдесят девять", 89),
                Arguments.arguments("четырнадцать", 14),
                Arguments.arguments("восемь", 8)
        };
    }

    @ParameterizedTest
    @MethodSource("defineDayByNumberTestArgs")
    public void defineDayByNumberTest(int input, String expected) {
        String actual = functions.defineDayByNumber(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("countDistanceTestArgs")
    public void countDistanceTest(long x1, long y1, long x2, long y2, double error, double expected) {
        double actual = functions.countDistance(x1, y1, x2, y2);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("convertNumToStrTestArgs")
    public void convertNumToStrTest1(long input, String expected) {
        String actual = functions.convertNumToStr(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("convertStrToNumTestArgs")
    public void convertStrToNumTest(String input, int expected) {
        int actual = functions.convertStrToNum(input);
        Assertions.assertEquals(expected, actual);
    }
}
