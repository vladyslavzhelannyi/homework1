package test.java.com.homework1;

import main.java.com.homework1.Cycles;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class CyclesTest {
    Cycles cycles = new Cycles();

    static Arguments[] defineIsPrimeTestArgs(){
        return new Arguments[]{
                Arguments.arguments(17, true),
                Arguments.arguments(24, false)
        };
    }

    static Arguments[] defineRootSequentialSelectionTestArgs(){
        return new Arguments[]{
                Arguments.arguments(9, 3),
                Arguments.arguments(28, 5)
        };
    }

    static Arguments[] defineRootBinarySearchTestArgs(){
        return new Arguments[]{
                Arguments.arguments(121, 11),
                Arguments.arguments(155, 12),
        };
    }

    static Arguments[] calculateFactorialTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 24),
                Arguments.arguments(3, 6)
        };
    }

    static Arguments[] countSumOfDigitsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(43571, 20),
                Arguments.arguments(0, 0),
                Arguments.arguments(-354, 12)
        };
    }

    static Arguments[] convertToMirrorNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(12345, 54321),
                Arguments.arguments(0, 0),
                Arguments.arguments(-3412, -2143)
        };
    }

    @Test
    public void findSumEvenFrom1To99Test(){
        int actual = cycles.findSumEvenFrom1To99();
        int expected = 2450;
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("defineIsPrimeTestArgs")
    public void defineIsPrimeTest(int input, boolean expected){
        boolean actual = cycles.defineIsPrime(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("defineRootSequentialSelectionTestArgs")
    public void defineRootSequentialSelectionTest(int input, int expected) {
        int actual = cycles.defineRootSequentialSelection(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("defineRootBinarySearchTestArgs")
    public void defineRootBinarySearchTest(int input, int expected){
        int actual = cycles.defineRootBinarySearch(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("calculateFactorialTestArgs")
    public void calculateFactorialTest(int input, int expected){
        int actual = cycles.calculateFactorial(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("countSumOfDigitsTestArgs")
    public void countSumOfDigitsTest(int input, int expected){
        int actual = cycles.countSumOfDigits(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("convertToMirrorNumberTestArgs")
    public void convertToMirrorNumberTest(int input, int expected){
        int actual = cycles.convertToMirrorNumber(input);
        Assertions.assertEquals(expected, actual);
    }
}
