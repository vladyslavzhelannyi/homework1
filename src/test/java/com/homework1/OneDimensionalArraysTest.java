package test.java.com.homework1;

import main.java.com.homework1.OneDimensionalArrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class OneDimensionalArraysTest {
    OneDimensionalArrays oneDimensionalArrays = new OneDimensionalArrays();

    static Arguments[] getMinItemTestArgs(){
        return new Arguments[]{
          Arguments.arguments(new int[]{-34, 5, 0, -109}, -109),
          Arguments.arguments(new int[]{5}, 5)
        };
    }

    static Arguments[] getMaxItemTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{6, 1, 0, -741, 435, -5, 32}, 435),
                Arguments.arguments(new int[]{-98}, -98)
        };
    }

    static Arguments[] getMinItemIndexTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{-9, 54, 34, -987, 445}, 3),
                Arguments.arguments(new int[]{97}, 0)
        };
    }

    static Arguments[] countSumOfNumsWithUnevenIndexTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{3, 567, 5, 0, -45, 5, 7}, -30),
                Arguments.arguments(new int[]{197}, 197)
        };
    }

    static Arguments[] reverseArrayTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{2, 5, 7, 1}, new int[]{1 , 7, 5, 2}),
                Arguments.arguments(new int[]{12}, new int[]{12}),
                Arguments.arguments(new int[]{13, 9, 231}, new int[]{231, 9, 13})
        };
    }

    static Arguments[] countUnevenNumQuantityTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{171, 19, 41}, 3),
                Arguments.arguments(new int[]{2, 194, 4, 12}, 0),
                Arguments.arguments(new int[]{22, 1, 24, 1235, 3}, 3),
                Arguments.arguments(new int[]{22, -23, 1}, 2),
                Arguments.arguments(new int[]{}, 0),
                Arguments.arguments(new int[]{1, -33, 0, 45, 2}, 3)
        };
    }

    static Arguments[] sortArrayBubbleTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{23}, new int[]{23}),
                Arguments.arguments(new int[]{23, -34, 23, 5, 0, -9}, new int[]{-34, -9, 0, 5, 23, 23}),
        };
    }

    static Arguments[] changeArrayHalfsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{3, 5, 2, 0, 12, 7}, new int[]{0, 12, 7, 3, 5, 2}),
                Arguments.arguments(new int[]{11, 4, 56, 7, 29}, new int[]{7, 29, 56, 11, 4}),
                Arguments.arguments(new int[]{1}, new int[]{1}),
                Arguments.arguments(new int[]{}, new int[]{})
        };
    }

    static Arguments[] sortArrayInsertTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{44}, new int[]{44}),
                Arguments.arguments(new int[]{45, -3, 0, 54, 45}, new int[]{-3, 0, 45, 45, 54}),
        };
    }

    static Arguments[] sortArraySelectTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{101}, new int[]{101}),
                Arguments.arguments(new int[]{6, -13, -200, 6, 0, 78, 12}, new int[]{-200, -13, 0, 6, 6, 12, 78}),
        };
    }

    static Arguments[] sortArrayHeapTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{7}, new int[]{7}),
                Arguments.arguments(new int[]{3, 4, 0, -4, 3, -9}, new int[]{-9, -4, 0, 3, 3, 4}),
        };
    }

    static Arguments[] sortArrayMergeTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{71}, new int[]{71}),
                Arguments.arguments(new int[]{0, -4, 7, 9, -1, 0, -12}, new int[]{-12, -4, -1, 0, 0, 7, 9}),
        };
    }

    static Arguments[] sortArrayQuickTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{222}, new int[]{222}),
                Arguments.arguments(new int[]{-13, -33, 0, 0, 34, 56, 1}, new int[]{-33, -13, 0, 0, 1, 34, 56}),
        };
    }

    static Arguments[] sortArrayShellTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{123}, new int[]{123}),
                Arguments.arguments(new int[]{-4, -2, 0, 4, -2, 9}, new int[]{-4, -2, -2, 0, 4, 9}),
        };
    }

    @ParameterizedTest
    @MethodSource("getMinItemTestArgs")
    public void getMinItemTest(int[] array, int expected){
        int actual = oneDimensionalArrays.getMinItem(array);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("getMaxItemTestArgs")
    public void getMaxItemTest(int[] array, int expected){
        int actual = oneDimensionalArrays.getMaxItem(array);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("getMinItemIndexTestArgs")
    public void getMinItemIndexTest(int[] array, int expected){
        int actual = oneDimensionalArrays.getMinItemIndex(array);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("countSumOfNumsWithUnevenIndexTestArgs")
    public void countSumOfNumsWithUnevenIndexTest(int[] array, int expected){
        int actual = oneDimensionalArrays.countSumOfNumsWithUnevenIndex(array);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("reverseArrayTestArgs")
    public void reverseArrayTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.reverseArray(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("countUnevenNumQuantityTestArgs")
    public void countUnevenNumQuantityTest(int[] array, int expected) {
        int actual = oneDimensionalArrays.countUnevenNumQuantity(array);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("changeArrayHalfsTestArgs")
    public void changeArrayHalfsTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.changeArrayHalfs(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArrayBubbleTestArgs")
    public void sortArrayBubbleTest1(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArrayBubble(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArrayInsertTestArgs")
    public void sortArrayInsertTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArrayInsert(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArraySelectTestArgs")
    public void sortArraySelectTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArraySelect(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArrayHeapTestArgs")
    public void sortArrayHeapTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArrayHeap(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArrayMergeTestArgs")
    public void sortArrayMergeTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArrayMerge(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArrayQuickTestArgs")
    public void sortArrayQuickTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArrayQuick(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("sortArrayShellTestArgs")
    public void sortArrayShellTest(int[] array, int[] expected) {
        int[] actual = oneDimensionalArrays.sortArrayShell(array);
        Assertions.assertArrayEquals(expected, actual);
    }
}
